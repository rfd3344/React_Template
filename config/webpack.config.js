const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './public/index.html',
  filename: 'index.html',
  inject: 'body'
})
const path = require('path');
const express = require('express')
var app = express()



module.exports = {
  entry: path.resolve(__dirname, '../src/index.js'),
  mode: 'production',
  output: {
    path: path.resolve(__dirname, '/public'),
    filename: 'index_bundle.js',
    publicPath: '/'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: [
      { test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: { loader: 'babel-loader' }
      },
      { test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'less-loader' }
        ]
      },
      { test: /\.(png|jpg|gif)$/,
        use: [ { loader: 'file-loader' }  ]
      }
    ]
  },
  resolve: {
    alias: {
      'src': path.resolve(__dirname, '../src/'),
      'components': path.resolve(__dirname, '../src/components'),
      'views': path.resolve(__dirname, '../src/views'),
      'utilis': path.resolve(__dirname, '../src/utilis'),
      'img': path.resolve(__dirname, '../src/img')
    }
  },
  plugins: [HtmlWebpackPluginConfig],
  devServer:{

    port: 3000,
    historyApiFallback: true,
    host: '0.0.0.0'
  }
}
