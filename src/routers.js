import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Page from 'src/layout/Page.js'

const Routers = () =>(
  <Router>
    <Switch>
      <Route path="/" component={Page} />
    </Switch>
  </Router>
)
export default Routers;
