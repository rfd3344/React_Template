//  All reducers
import { combineReducers } from 'redux'

import Example from 'views/Example/Redux.js'

/*********************************/

export default combineReducers({
  Example
})
