import React from 'react';
import { Route } from 'react-router-dom';

import './page.less'
import Header from './Header'
import Footer from './Footer'
import Home from 'views/Home'
import Example from 'views/Example'


const Page =() =>(
  <div>
    <Header />
    <main id="page">
      <h1> Page Template</h1>
      <Route exact path="/" component={Home} />
      <Route exact path="/example" component={Example} />
    </main>
    <Footer />
  </div>
)

export default Page
